import os
import qrcode
from telegram.ext import Updater, CommandHandler, ConversationHandler, MessageHandler, Filters
from telegram import InlineKeyboardMarkup, InlineKeyboardButton

INPUT_TEXT = 0

def start(update, context):

    button1 = InlineKeyboardButton(
        text='Sobre el autor',
        url='https://gitlab.com/angel9876/telegrambot/-/tree/main'
    )

    button2 = InlineKeyboardButton(
        text='Fecebook',
        url='https://www.facebook.com/profile.php?id=100008552191605'
    )

    update.message.reply_text(
        text = 'Has clic en un botón',
        reply_markup=InlineKeyboardMarkup([
            [button1],
            [button2]
        ])
    )

if __name__ == '__main__':

    updater = Updater(token='5097630315:AAHkyO3joTzBLaZbLsqs-z3DelOhkGu5P1I', use_context= True)

    dp = updater.dispatcher
    dp.add_handler(CommandHandler('start', start))

    updater.start_polling()
    updater.idle()
