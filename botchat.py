import os
import qrcode
import pyshorteners
from telegram.ext import Updater, CommandHandler, ConversationHandler, CallbackQueryHandler
from telegram.ext import MessageHandler, Filters
from telegram import ChatAction
from telegram import InlineKeyboardMarkup, InlineKeyboardButton

INPUT_TEXT = 0
INPUT_URL = 0

def start(update, context):

    update.message.reply_text(
        text='Hola, bienvenido.\n ¿Qué deseas hacer?',

        #Botones en Telegram
        reply_markup=InlineKeyboardMarkup([
            [InlineKeyboardButton(text='Generar QR', callback_data='qr')],
            [InlineKeyboardButton(text='Acortar Url', callback_data='url')],
            [InlineKeyboardButton(text='Sobre el autor', url='https://www.facebook.com/profile.php?id=100008552191605')]
        ])
    )

def qr_command_handler(update, context):

    update.message.reply_text('Envíame el texto para generar un código QR.')
    return INPUT_TEXT


def qr_callback_handler(update, context):

    query_qr = update.callback_query
    query_qr.answer()

    query_qr.edit_message_text(
        text=('Envíame el texto para generar un código QR.')
    )

    return INPUT_TEXT

def url_callback_handler(update, context):

    query_url = update.callback_query
    query_url.answer()

    query_url.edit_message_text(
        text=('Envíame un enlace para acortarlo.')
    )

    return INPUT_URL


def generate_qr(text):

    filename = text + '.jpg'

    img = qrcode.make(text)
    img.save(filename)

    return filename


def send_qr(filename, chat):

    chat.send_action(
        action=ChatAction.UPLOAD_PHOTO,
        timeout=None
    )

    chat.send_photo(
        photo=open(filename, 'rb')
    )

    os.unlink(filename)

def input_text(update, context):

    text =update.message.text
    filename = generate_qr(text)
    chat = update.message.chat
    send_qr(filename, chat)

    return ConversationHandler.END

def input_url(update, context):

    url = update.message.text
    chat = update.message.chat

    #acortar url
    s = pyshorteners.Shortener()
    short = s.chilpit.short(url)

    chat.send_action(
        action=ChatAction. TYPING,
        timeout=None
    )

    chat.send_message(
        text=short
    )

    return ConversationHandler.END


if __name__ == '__main__':

    updater = Updater(token='5097630315:AAHkyO3joTzBLaZbLsqs-z3DelOhkGu5P1I', use_context= True)

    dp = updater.dispatcher
    dp.add_handler(CommandHandler('start', start))

    dp.add_handler(ConversationHandler(
        entry_points=[
            CommandHandler('qr', qr_command_handler),
            CallbackQueryHandler(pattern='qr', callback=qr_callback_handler)
        ],

        states={
            INPUT_TEXT: [MessageHandler(Filters.text, input_text)]
        },

        fallbacks=[]
    ))

    dp.add_handler(ConversationHandler(
        entry_points=[
            CallbackQueryHandler(pattern='url', callback=url_callback_handler)
        ],

        states={
            INPUT_URL: [MessageHandler(Filters.text, input_url)]
        },

        fallbacks=[]
    ))

    updater.start_polling()
    updater.idle()
