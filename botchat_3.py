import pyshorteners
from telegram.ext import Updater, CommandHandler, ConversationHandler, CallbackQueryHandler
from telegram.ext import MessageHandler, Filters
from telegram import ChatAction
from telegram import InlineKeyboardMarkup, InlineKeyboardButton

INPUT_URL = 0

def start(update, context):

    update.message.reply_text(
        text='Hola, bienvenido.\n ¿Qué deseas hacer?',

        #botones presentes depues de iniciar
        reply_markup=InlineKeyboardMarkup([
            [InlineKeyboardButton(text='Acortar Url', callback_data='url')]
        ])
    )


def url_callback_handler(update, context):

    query = update.callback_query
    query.answer()

    query.edit_message_text(
        text=('Envíame un enlace para acortarlo.')
    )

    return INPUT_URL



def input_url(update, context):

    url = update.message.text
    chat = update.message.chat

    #acortar url
    s = pyshorteners.Shortener()
    short = s.chilpit.short(url)

    chat.send_action(
        action=ChatAction. TYPING,
        timeout=None
    )

    chat.send_message(
        text=short
    )

    return ConversationHandler.END


if __name__ == '__main__':

    updater = Updater(token='5097630315:AAHkyO3joTzBLaZbLsqs-z3DelOhkGu5P1I', use_context= True)

    dp = updater.dispatcher
    dp.add_handler(CommandHandler('start', start))

    dp.add_handler(ConversationHandler(
            entry_points=[
                CallbackQueryHandler(pattern='url', callback=url_callback_handler)
            ],

            states={
                INPUT_URL: [MessageHandler(Filters.text, input_url)]
            },

        fallbacks=[]
    ))

    updater.start_polling()
    updater.idle()
